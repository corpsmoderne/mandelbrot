
NVCC = nvcc

SRC_C = mandel.c
SRC_GPU = mandel.gpu.cu
SRC_CPU = mandel.cpu.c

OBJ_GPU = $(SRC_GPU:.cu=.o) $(SRC_C:.c=.o)
OBJ_CPU = $(SRC_CPU:.c=.o) $(SRC_C:.c=.o)

NAME_GPU = mandel.gpu
NAME_CPU = mandel.cpu

CFLAGS = -Wall -O3

LDFLAGS = -lcsfml-graphics -lcsfml-system -lm 

all : $(NAME_GPU) $(NAME_CPU)

$(NAME_GPU) : LDFLAGS += -lcudart -lstdc++
$(NAME_GPU) : $(OBJ_GPU)
		$(CC) $(OBJ_GPU) -o $(NAME_GPU) $(LDFLAGS)

$(NAME_CPU)	: $(OBJ_CPU)
	$(CC) $(OBJ_CPU) $(LDFLAGS) -lm -o $(NAME_CPU)

%.o : %.cu
	$(NVCC) -D_FORCE_INLINES -O3 -c -o $@ $<

clean:
	$(RM) $(OBJ_GPU) $(OBJ_CPU) *~
fclean: clean
	$(RM) $(NAME_GPU) $(NAME_CPU)

re: fclean all

.PHONY: clean fclean


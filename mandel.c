#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <SFML/Graphics.h>
#include <SFML/System.h>
#include "mandel.h"

static int init_context(struct context *ctx)
{
  ctx->window = sfRenderWindow_create(ctx->mode, "Mandel", sfClose, NULL);
  if (ctx->window == NULL) {
    printf("window is null :(\n");
    return 0;
  }

  ctx->texture = sfTexture_create(ctx->mode.width, ctx->mode.height);
  if (ctx->texture == NULL) {
    return 0;
  }

  ctx->sprite = sfSprite_create();
  if ((ctx->sprite = sfSprite_create()) == NULL) {
    return 0;
  }

  sfSprite_setTexture(ctx->sprite, ctx->texture, sfTrue);

  ctx->pixels = (unsigned char *) malloc(sizeof(unsigned char)*4*
                                         ctx->mode.width*ctx->mode.height);
  return 1;
}

static int poll_events(struct context *ctx,
		       struct vec2f *pos, float *size, int *it_max)
{
  sfEvent event;
  float x;
  float y;
  while (sfRenderWindow_pollEvent(ctx->window, &event)) {
    switch (event.type) {
    case sfEvtClosed:
      sfRenderWindow_close(ctx->window);
      break;
    case sfEvtMouseMoved:
      printf("%i %i >> %i\n", event.mouseMove.x, event.mouseMove.y,
             ctx->values[event.mouseMove.x+
                         (event.mouseMove.y*ctx->mode.width)]);
      break;
    case sfEvtMouseButtonPressed:
      if (event.mouseButton.button == 0) {
        x = (float)event.mouseButton.x;
        y = (float)event.mouseButton.y;
        printf("%f %f\n", x-0.5, y-0.5);
        
        pos->x += (x - ctx->mode.width/2) / *size;
        pos->y += (y - ctx->mode.height/2) / *size;
        *size *= 10;
      } else if (event.mouseButton.button == 1) {
        *size /= 10;
      }
      break;
    case sfEvtKeyPressed:
      switch(event.key.code) {
      case sfKeyEscape:
        sfRenderWindow_close(ctx->window);
        break;
      case sfKeyUp:
        pos->y -= 10 / *size;
        break;
      case sfKeyDown:
        pos->y += 10 / *size;
        break;
      case sfKeyLeft:
        pos->x -= 10 / *size;
        break;
      case sfKeyRight:
        pos->x += 10 / *size;
        break;
      case sfKeyEqual:
        *size *= 2;
        break;
      case 56: //sfKeySubtract: ???
        *size /= 2;
        break;
      case sfKeyPageUp:
        *it_max *= 2;
        break;
      case sfKeyPageDown:
        *it_max /= 2;
        break;
      default:
        printf("%i\n", event.key.code);
        break;
      }
      printf("x:%f y:%f // s:%f i:%i\n", pos->x, pos->y, *size, *it_max);
      break;
    default:
      break;
    }
  }
  if (*size < 128) {
    *size = 128;
  }
  return 1;
}

static void set_pixel(unsigned char *pixels,
		      unsigned char r, unsigned char g, unsigned char b)
{
        pixels[0] = r;
        pixels[1] = g;
        pixels[2] = b;
        pixels[3] = 255;
}

void draw(struct context *ctx, struct vec2f *pos,
          float size, int it_max)
{
  if (compute(ctx, pos, size, it_max) == 0) {
    return;
  }
  
  int c;
  int r, g, b;
  float minc = it_max*1000;
  float maxc = -it_max;
  
  for(int y=0; y < ctx->mode.height; ++y) {
    for(int x=0; x < ctx->mode.width; ++x) {
      //c = (int) (log10(ctx->values[x+(y*ctx->mode.width)]) * it_max) % 254;
      //c = 255 / sqrt(1+((float)ctx->values[x+(y*ctx->mode.width)] / it_max)) ;
      c = ctx->values[x+(y*ctx->mode.width)] % 256;
      minc = fmin(c, minc);
      maxc = fmax(c, maxc);
      r = 0;
      g = 0;
      b = 0;
      if (c < 128) {
        r = (128-c)*2;
        b = c*2;
      } else {
        b = (128-c)*2;
        g = c*2;
      }
      set_pixel(ctx->pixels + (4*(x+(y*ctx->mode.width))),
                r%255, g%255, b%255);
    }
  }
  //printf("%f %f\n", minc, maxc);
}

static int run_main_loop(struct context *ctx)
{
  int finished = 0;

  float size = 240;
  float size_ = 1;
  int it_max = 512;

  struct vec2f pos;
  struct vec2f pos_to = { 0, 0 };

  pos.x = 0.302083;
  pos.y = 0.023438;
  
  sfClock *clock = sfClock_create();
  sfTime t1 = sfClock_getElapsedTime(clock);
  sfTime t2 = t1;
  float fps = 0;
  float dt = 0;
  sfColor color = {
    255, 255, 255, 255
  };
  sfFont *font = sfFont_createFromFile("arial.ttf");
  sfText *text = sfText_create();

  sfText_setColor(text, color);
  sfText_setFont(text, font);
  sfText_setCharacterSize(text, 12);
  char buff[128];
  
  while (!finished && sfRenderWindow_isOpen(ctx->window)) {

    t2 = sfClock_getElapsedTime(clock);
    dt = (sfTime_asSeconds(t2)-sfTime_asSeconds(t1));
    fps = 1/dt;
    sprintf(buff, "%.2f fps\n", fps);
    sfText_setString(text, buff);
    t1 = t2;

    if (fabs(size-size_) > 1) {
      size_ += (size-size_) / 10;
    }

    if (fabs((pos.x*size_)-(pos_to.x*size_)) > 1) {
      pos.x += (pos_to.x-pos.x) / 2 ;
    }
    if (fabs((pos.y*size_)-(pos_to.y*size_)) > 1) {
      pos.y += (pos_to.y-pos.y) / 2 ;
    }
    
    if (!finished) {
      draw(ctx, &pos, size_, it_max);
      
      sfTexture_updateFromPixels(ctx->texture, ctx->pixels,
				 ctx->mode.width,
				 ctx->mode.height, 0, 0);
    }
    poll_events(ctx, &pos_to, &size, &it_max);
    
    sfRenderWindow_clear(ctx->window, sfBlack);
    sfRenderWindow_drawSprite(ctx->window, ctx->sprite, NULL);
    sfRenderWindow_drawText(ctx->window, text, NULL);
    sfRenderWindow_display(ctx->window);
  }
  return 1;
}

int main(int ac, char **av)
{
  struct context *ctx = alloc_context(1200, 800);
  
  if (init_context(ctx) == 0) {
    printf("unable to init everything, dhaaa :(\n");
    return -42;
  }
  run_main_loop(ctx);

  free(ctx->pixels);
  free_context(ctx);
  return 0;
}

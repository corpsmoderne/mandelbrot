#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <SFML/Graphics.h>
#include "mandel.h"

struct context *alloc_context(int width, int height)
{
  struct context *ctx;
  
  ctx = malloc(sizeof(struct context));
  ctx->mode.width = width;
  ctx->mode.height = height;
  ctx->mode.bitsPerPixel = 32;

  ctx->values = malloc(ctx->mode.width*ctx->mode.height*sizeof(int));
  for(int i=0; i < ctx->mode.width*ctx->mode.height; ++i) {
    ctx->values[i] = 0;
  }
  return ctx;
}

void free_context(struct context *ctx)
{
  free(ctx->values);
  free(ctx);
}

int compute(struct context *ctx, struct vec2f *pos,
            float size, int it_max)
{
  double X = pos->x; //ctx->mode.width/2;
  double Y = pos->y; //ctx->mode.height/2;
  double S = size; //ctx->mode.width;
  int MAX_I = it_max;
  
  double xx;
  double yy;
  double x_;
  double y_;
  double x_tmp;
  int i;
  
  for(int y=0; y < ctx->mode.height; ++y) {
    for(int x=0; x < ctx->mode.width; ++x) {
      i=0;
      xx = X+((float)x-ctx->mode.width/2)/S;
      yy = Y+((float)y-ctx->mode.height/2)/S;
      x_ = 0;
      y_ = 0;
      
      while(x_*x_+y_*y_ < 10 && i < MAX_I) {
        x_tmp = x_*x_ - y_*y_ + xx;
        y_ = 2 * x_ * y_ + yy;
        x_ = x_tmp;
        ++i;
      }
      ctx->values[x+(y*ctx->mode.width)] = i;
    }
  }
  return 1;
}

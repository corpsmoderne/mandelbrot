#include <stdio.h>
#include <SFML/Graphics.h>
extern "C" {
  #include "mandel.h"
}

#define N 256

struct context *alloc_context(int width, int height)
{
  struct context *ctx;

  cudaMallocManaged(&ctx, sizeof(struct context));
  ctx->mode.width = width;
  ctx->mode.height = height;
  ctx->mode.bitsPerPixel = 32;

  cudaMallocManaged(&ctx->values, //n * sizeof(int));
                    ctx->mode.width*ctx->mode.height*sizeof(int));
  return ctx;
}

void free_context(struct context *ctx)
{
  cudaFree(ctx->values);
  cudaFree(ctx);
}

__global__
void run(struct context *ctx, double X, double Y, float size, int it_max)
{
  int n = threadIdx.x + blockIdx.x * N;
  int x = n % ctx->mode.width;
  int y = n / ctx->mode.width;

  double xx = X+((double)x-ctx->mode.width/2)/size;
  double yy = Y+((double)y-ctx->mode.height/2)/size;
  double x_ = 0;
  double y_ = 0;
  double x_tmp;
  int i = 0;

  double x2 = x_*x_;
  double y2 = y_*y_;
  
  while(x2+y2 < 4 && i < it_max) {
    x_tmp = x2 - y2 + xx;
    y_ = (x_+x_) * y_ + yy;
    x_ = x_tmp;

    x2 = x_*x_;
    y2 = y_*y_;
    ++i;
  }
  ctx->values[x+(y*ctx->mode.width)] = i;
}

int compute( struct context *ctx, struct vec2f *pos,
              float size, int it_max )
{
  cudaError err;

  run<<<(ctx->mode.height*ctx->mode.width)/N, N>>> (ctx,pos->x, pos->y,
                                                    size, it_max);
  
  err = cudaDeviceSynchronize();
  if (err != cudaSuccess) {
    printf("error => %s\n", cudaGetErrorString( err ));
    return 0;
  }
  return 1;
}

struct context {
  sfVideoMode mode;
  sfRenderWindow *window;
  sfTexture *texture;
  sfSprite *sprite;
  unsigned char *pixels;
  int *values;
};

struct vec2f {
  double x;
  double y;
};

struct context *alloc_context( int width, int height );
void free_context( struct context *ctx );
int compute( struct context *ctx, struct vec2f *pos,
            float size, int it_max );
